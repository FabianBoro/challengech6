package id.com.fabianboro.myapplication.models

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
