package id.com.fabianboro.myapplication.service

import id.com.fabianboro.myapplication.models.DetailMovie
import id.com.fabianboro.myapplication.models.PopularMovie
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("movie/popular")
    fun getPopularMovie(@Query("api_key") key: String): Call<PopularMovie>

    @GET("movie/{movie_id}")
    fun getDetailMovie(@Path("movie_id") id: Int, @Query("api_key") key: String): Call<DetailMovie>
}
