package id.com.fabianboro.myapplication.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class FavoriteMovie(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    @ColumnInfo(name = "accountId") val accoundId: Int,
    @ColumnInfo(name = "movieId") val movieId: Int
)
