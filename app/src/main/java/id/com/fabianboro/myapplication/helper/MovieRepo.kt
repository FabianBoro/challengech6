package id.com.fabianboro.myapplication.helper

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import id.com.fabianboro.myapplication.models.DetailMovie
import id.com.fabianboro.myapplication.models.ResultPopularMovie
import id.com.fabianboro.myapplication.BuildConfig
import id.com.fabianboro.myapplication.service.ApiClient
import id.com.fabianboro.myapplication.service.ApiService

class MovieRepo {
    private val apiService: ApiService by lazy { ApiClient.instance }

    suspend fun getDataDetailMovieFromNetwork(movieId: Int) =
        apiService.getDetailMovie(movieId, BuildConfig.API_KEY)

    suspend fun getDataListMovieFromNetwork() = apiService.getPopularMovie(BuildConfig.API_KEY)
}
