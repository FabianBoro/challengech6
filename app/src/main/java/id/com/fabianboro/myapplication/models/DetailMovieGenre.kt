package id.com.fabianboro.myapplication.models

import com.google.gson.annotations.SerializedName

data class DetailMovieGenre(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)
