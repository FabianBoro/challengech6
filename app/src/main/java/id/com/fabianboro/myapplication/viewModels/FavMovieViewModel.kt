package id.com.fabianboro.myapplication.viewModels

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.com.fabianboro.myapplication.helper.AccountRepo
import id.com.fabianboro.myapplication.helper.MovieRepo
import kotlinx.coroutines.launch

class FavoriteMovieViewModel(context: Context): ViewModel() {
    private val movieRepo: MovieRepo by lazy { MovieRepo() }
    private val accountRepo: AccountRepo by lazy { AccountRepo(context) }

    fun getData(accountId: Int) {
        viewModelScope.launch {
            val movieId = accountRepo.getFavoriteMovieList(accountId)
            if (movieId!= null) {
                for ( id in movieId){

                }
            }
            movieRepo.getDataListMovieFromNetwork()
        }
    }
}
