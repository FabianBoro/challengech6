package id.com.fabianboro.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.com.fabianboro.myapplication.databinding.FragmentLoginBinding
import id.com.fabianboro.myapplication.helper.AccountRepo
import id.com.fabianboro.myapplication.helper.SharedPref
import id.com.fabianboro.myapplication.helper.viewModelsFactory
import id.com.fabianboro.myapplication.viewModels.LoginViewModel

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private var accountIdPref: Int = 0
    private var usernamePref: String = ""
    private var emailPref: String = ""

    private val accountRepo: AccountRepo by lazy { AccountRepo(requireContext()) }

    private val viewModel: LoginViewModel by viewModelsFactory { LoginViewModel(accountRepo) }

    private val sharedPref: SharedPref by lazy { SharedPref(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvToRegisterClicked()
        btnLoginClicked()
        observeData()
    }


    private fun btnLoginClicked() {
        binding.apply {
            btnLogin.setOnClickListener {
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()
                if (email.isEmpty()) {
                    tilEmail.error = "Email harus diisi"
                } else {
                    tilEmail.error = null
                }
                if (password.isEmpty()) {
                    tilPassword.error = "Password harus diisi"
                } else {
                    tilPassword.error = null
                }
                if (email.isNotEmpty() && password.isNotEmpty()) {
                    viewModel.loginAction(email, password)
                }
            }
        }
    }

    private fun observeData() {
        viewModel.accountRegistered.observe(viewLifecycleOwner) {
            if (it == true) {
                createToast("Login Berhasil").show()
                loginToHomeFragment()
            } else {
                createToast("Email atau Password Salah").show()
            }
        }
        viewModel.accountIdPref.observe(viewLifecycleOwner) {
            accountIdPref = it
        }
        viewModel.usernamePref.observe(viewLifecycleOwner) {
            usernamePref = it
        }
        viewModel.emailPref.observe(viewLifecycleOwner) {
            emailPref = it
        }
    }

    private fun loginToHomeFragment() {
        sharedPref.setDataPrefAccount(accountIdPref, usernamePref, emailPref)
        findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
    }

    private fun tvToRegisterClicked() {
        binding.apply {
            tvToRegister.setOnClickListener {
                binding.etEmail.text?.clear()
                binding.etPassword.text?.clear()
                it.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
            }
        }
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_LONG)
    }
}
