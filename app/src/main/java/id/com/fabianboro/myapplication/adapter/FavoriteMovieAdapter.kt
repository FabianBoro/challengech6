package id.com.fabianboro.myapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.com.fabianboro.myapplication.databinding.ItemMovieBinding
import id.com.fabianboro.myapplication.helper.toDate
import id.com.fabianboro.myapplication.models.ResultPopularMovie

class FavoriteMovieAdapter(private var layoutClicked: (ResultPopularMovie) -> Unit) : RecyclerView.Adapter<FavoriteMovieAdapter.FavoriteMovieViewHolder>(){
    private val diffCallback = object : DiffUtil.ItemCallback<ResultPopularMovie>() {
        override fun areContentsTheSame(
            oldItem: ResultPopularMovie,
            newItem: ResultPopularMovie
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areItemsTheSame(
            oldItem: ResultPopularMovie,
            newItem: ResultPopularMovie
        ): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }
    }

    private val listDiffer = AsyncListDiffer(this, diffCallback)

    fun updateData(movies: List<ResultPopularMovie>) = listDiffer.submitList(movies)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteMovieViewHolder {
        val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FavoriteMovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FavoriteMovieViewHolder, position: Int) {
        holder.bind(listDiffer.currentList[position])
    }

    override fun getItemCount(): Int = listDiffer.currentList.size

    inner class FavoriteMovieViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ResultPopularMovie) {
            binding.apply {
                tvMovieTitle.text = item.title
                tvMovieContent.text = item.releaseDate.toDate()
                val rating = item.voteAverage * 10
                tvRating.text = rating.toInt().toString()

                layoutItem.setOnClickListener {
                    layoutClicked.invoke(item)
                }
                val posterLink = item.posterPath
                Glide.with(itemView.context).load("https://image.tmdb.org/t/p/original$posterLink")
                    .into(ivMovie)
            }
        }
    }


}
