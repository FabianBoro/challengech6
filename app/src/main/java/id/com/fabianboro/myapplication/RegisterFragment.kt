package id.com.fabianboro.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.findNavController
import id.com.fabianboro.myapplication.databinding.FragmentRegisterBinding
import id.com.fabianboro.myapplication.helper.AccountRepo
import id.com.fabianboro.myapplication.helper.viewModelsFactory
import id.com.fabianboro.myapplication.viewModels.RegisterViewModel

class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private var username: String = ""
    private var email: String = ""
    private var password: String = ""

    private val accountRepo: AccountRepo by lazy { AccountRepo(requireContext()) }

    private val viewModel: RegisterViewModel by viewModelsFactory { RegisterViewModel(accountRepo) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkEditTextOnChange()
        registerBtnClicked()
        observeData()
    }

    private fun checkEditTextOnChange() {
        binding.apply {
            etUsername.doAfterTextChanged {
                if (etUsername.text.toString().isEmpty()) {
                    tilUsername.error = "Username harus diisi"
                } else {
                    tilUsername.error = null
                }
            }
            etEmail.doAfterTextChanged {
                if (etEmail.text.toString().isEmpty()) {
                    tilEmail.error = "Email harus diisi"
                } else {
                    tilEmail.error = null
                }
            }
            etPassword.doAfterTextChanged {
                if (etPassword.text.toString().isEmpty()) {
                    tilPassword.error = "Password harus diisi"
                } else {
                    tilPassword.error = null
                }
            }
            etPasswordConfirm.doAfterTextChanged {
                if (etPasswordConfirm.text.toString().isEmpty()) {
                    tilPasswordConfirm.error = "Password harus diisi"
                } else if (checkConfirmPassword()) {
                    tilPasswordConfirm.error = null
                } else {
                    tilPasswordConfirm.error = "Password tidak sesuai"
                }
            }
        }

    }

    private fun checkConfirmPassword(): Boolean {
        binding.apply {
            return etPassword.text.toString() == etPasswordConfirm.text.toString()
        }
    }

    private fun registerBtnClicked() {
        binding.apply {
            btnRegister.setOnClickListener {
                username = etUsername.text.toString()
                email = etEmail.text.toString()
                password = etPassword.text.toString()
                val passwordConfirm = etPasswordConfirm.text.toString()
                when {
                    username.isEmpty() -> {
                        tilUsername.error = "Username harus diisi"
                    }
                    email.isEmpty() -> {
                        tilEmail.error = "Email harus diisi"
                    }
                    password.isEmpty() -> {
                        tilPassword.error = "Password harus diisi"
                    }
                    passwordConfirm.isEmpty() -> {
                        tilPasswordConfirm.error = "Password harus diisi"
                    }
                    password != passwordConfirm -> {
                        tilPasswordConfirm.error = "Password tidak sesuai"
                    }
                    else -> {
                        viewModel.checkRegisteredEmail(email)
                    }
                }
            }
        }
    }

    private fun observeData() {
        viewModel.accountRegistered.observe(viewLifecycleOwner) {
            if (it == true) {
                createToast("Email Sudah Terdaftar").show()
            } else {
                viewModel.saveToDb(username, email, password)
            }
        }
        viewModel.accountAdded.observe(viewLifecycleOwner) {
            if (it == true) {
                registerToLoginFragment()
                createToast("Register Berhasil")
            }
        }
    }

    private fun registerToLoginFragment() {
        findNavController().popBackStack()
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_LONG)
    }
}
