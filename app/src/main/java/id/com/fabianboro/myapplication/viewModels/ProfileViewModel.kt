package id.com.fabianboro.myapplication.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.com.fabianboro.myapplication.database.Account
import id.com.fabianboro.myapplication.helper.AccountRepo
import kotlinx.coroutines.launch

class ProfileViewModel(private val accountRepo: AccountRepo) : ViewModel() {
    private var _account = MutableLiveData<Account>()
    val account: LiveData<Account> get() = _account

    val updatedData = MutableLiveData<Boolean>()
    val updatedUsername = MutableLiveData<String>()

    fun getProfileData(accountId: Int) {
        viewModelScope.launch {
            val result = accountRepo.getRegisteredAccountId(accountId)
            if (result != null) {
                _account.postValue(result[0])
            }
        }
    }

    fun updateData(
        accountId: Int,
        username: String,
        fullname: String,
        birthdate: String,
        address: String
    ) {
        viewModelScope.launch {
            val result =
                accountRepo.updateProfileAccount(accountId, username, fullname, birthdate, address)
            if (result != 0) {
                updatedData.value = true
                updatedUsername.value = username
            } else {
                updatedData.value = false
            }
        }
    }
}
