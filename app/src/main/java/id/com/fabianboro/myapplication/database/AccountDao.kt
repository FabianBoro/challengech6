package id.com.fabianboro.myapplication.database

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import id.com.fabianboro.myapplication.database.Account

@Dao
interface AccountDao {
    @Query("SELECT * FROM Account")
    fun getAllAccount(): List<Account>

    @Query("SELECT * FROM Account WHERE email = :email AND password = :password")
    fun getRegisteredAccount(email: String?, password: String?): List<Account>

    @Query("SELECT * FROM Account WHERE email = :email")
    fun getRegisteredAccountEmail(email: String?): List<Account>

    @Query("SELECT * FROM Account WHERE id = :id")
    fun getRegisteredAccountId(id: Int): List<Account>

    @Query(
        "UPDATE Account SET username = :username, " +
                "fullname = :fullname, " +
                "birthdate = :birthdate, " +
                "address = :address WHERE id = :id"
    )
    fun updateProfileAccount(
        id: Int,
        username: String,
        fullname: String,
        birthdate: String,
        address: String
    ): Int

    @Insert(onConflict = REPLACE)
    fun insertAccount(account: Account): Long

    @Update
    fun updateAccount(account: Account): Int

    @Delete
    fun deleteAccount(account: Account): Int
}

