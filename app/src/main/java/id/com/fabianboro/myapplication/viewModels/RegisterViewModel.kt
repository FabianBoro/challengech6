package id.com.fabianboro.myapplication.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.com.fabianboro.myapplication.helper.AccountRepo
import kotlinx.coroutines.launch

class RegisterViewModel(private val accountRepo: AccountRepo) : ViewModel() {
    val accountRegistered = MutableLiveData<Boolean>()
    val accountAdded = MutableLiveData<Boolean>()

    fun checkRegisteredEmail(email: String) {
        viewModelScope.launch {
            val result = accountRepo.getRegisteredAccountWithEmail(email)
            accountRegistered.value = !result.isNullOrEmpty()
        }
    }

    fun saveToDb(username: String, email: String, password: String) {
        val account = Account(null, username, email, password, username, "", "", null)
        viewModelScope.launch {
            val result = accountRepo.insertAccount(account)
            if (result != 0L) {
                accountAdded.value = true
            }
        }
    }
}
