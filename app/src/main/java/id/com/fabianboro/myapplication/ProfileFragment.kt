package id.com.fabianboro.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import id.com.fabianboro.myapplication.databinding.FragmentProfileBinding
import id.com.fabianboro.myapplication.helper.AccountRepo
import id.com.fabianboro.myapplication.helper.SharedPref
import id.com.fabianboro.myapplication.helper.viewModelsFactory
import id.com.fabianboro.myapplication.viewModels.ProfileViewModel

class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private val accountRepo: AccountRepo by lazy { AccountRepo(requireContext()) }

    private val sharedPref: SharedPref by lazy { SharedPref(requireContext()) }

    private val viewModel: ProfileViewModel by viewModelsFactory { ProfileViewModel(accountRepo) }

    private var accountId: Int = 0
    private var email: String = ""
    private var password: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        accountId = sharedPref.getDataId()
        viewModel.getProfileData(accountId)
        observeData()
        btnUpdateClicked()
        btnLogoutClicked()
    }

    private fun observeData() {
        viewModel.account.observe(viewLifecycleOwner) {
            binding.apply {
                etUsername.setText(it.username)
                etFullname.setText(it.fullname)
                etBirthdate.setText(it.birthdate)
                etAddress.setText(it.address)

                email = it.email
                password = it.password
            }
        }
        viewModel.updatedData.observe(viewLifecycleOwner) {
            if (it == true) {
                createToast("Berhasil Update Profil").show()
                profileToHomeFragment()
            } else {
                createToast("Berhasil Gagal Profil").show()
            }
        }
        viewModel.updatedUsername.observe(viewLifecycleOwner) {
            val username = it
            sharedPref.setDataUsernamePrefAccount(username)

        }
    }

    private fun btnUpdateClicked() {
        binding.apply {
            btnUpdateProfile.setOnClickListener {
                val username = etUsername.text.toString()
                val fullname = etFullname.text.toString()
                val birthdate = etBirthdate.text.toString()
                val address = etAddress.text.toString()
                when {
                    username.isEmpty() -> {
                        etUsername.error = "Username harus diisi"
                    }
                    fullname.isEmpty() -> {
                        etFullname.error = "Email harus diisi"
                    }
                    else -> {
                        viewModel.updateData(accountId, username, fullname, birthdate, address)
                    }
                }
            }
        }
    }

    private fun profileToHomeFragment() {
        findNavController().popBackStack()
    }

    private fun btnLogoutClicked() {
        binding.btnLogout.setOnClickListener {
            logoutAction()
        }
    }

    private fun logoutAction() {
        sharedPref.deleteDataPrefAccount()
        Toast.makeText(requireContext(), "Logout Berhasil", Toast.LENGTH_SHORT).show()
        findNavController().navigate(R.id.action_profileFragment_to_loginFragment)
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_LONG)
    }
}
