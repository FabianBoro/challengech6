package id.com.fabianboro.myapplication.models

import com.google.gson.annotations.SerializedName

data class DetailMovieSpokenLanguage(
    @SerializedName("iso_639_1")
    val iso6391: String,
    @SerializedName("name")
    val name: String
)

