package id.com.fabianboro.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.com.fabianboro.myapplication.adapter.MovieAdapter
import id.com.fabianboro.myapplication.databinding.FragmentFavoriteMovieBinding
import id.com.fabianboro.myapplication.viewModels.HomeViewModel

class FavoriteMovieFragment : Fragment() {
    private var _binding : FragmentFavoriteMovieBinding? = null
    private val binding get() = _binding!!

    private lateinit var movieAdapter: MovieAdapter

    private val viewModel : HomeViewModel by lazy { HomeViewModel(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFavoriteMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
    }

    private fun initRecyclerView() {
        binding.apply {
            movieAdapter = MovieAdapter {
                val movieId = it.id
                val bundle = Bundle()
                bundle.putInt("KEY_ID", movieId)
                findNavController().navigate(
                    R.id.action_homeFragment_to_detailMovieFragment,
                    bundle
                )
            }
            rvFavorite.adapter = movieAdapter
            rvFavorite.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun observeData() {
    }
}
