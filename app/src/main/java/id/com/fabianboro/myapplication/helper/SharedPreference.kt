package id.com.fabianboro.myapplication.helper

import android.content.Context

class SharedPref(context: Context) {
    private val sharedPreference = context.getSharedPreferences("login", Context.MODE_PRIVATE)

    fun setDataPrefAccount(accountId: Int, username: String, email: String) {
        val editor = sharedPreference.edit()
        editor.putBoolean(isLoginKey, true)
        editor.putInt(accountIdKey, accountId)
        editor.putString(usernameKey, username)
        editor.putString(emailKey, email)
        editor.apply()
    }

    fun setDataUsernamePrefAccount(username: String) {
        val editor = sharedPreference.edit()
        editor.putString(usernameKey, username)
        editor.apply()
    }

    fun getStatusLogin(): Boolean {
        return sharedPreference.getBoolean(isLoginKey, false)
    }

    fun getDataUsername(): String {
        return sharedPreference.getString(usernameKey, "default").toString()
    }

    fun getDataEmail(): String {
        return sharedPreference.getString(emailKey, "default").toString()
    }

    fun getDataId(): Int {
        return sharedPreference.getInt(accountIdKey, 0)
    }

    fun deleteDataPrefAccount() {
        val editor = sharedPreference.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        const val isLoginKey = "isLogin"
        const val usernameKey = "username"
        const val accountIdKey = "accountId"
        const val emailKey = "email"
    }
}
