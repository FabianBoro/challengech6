package id.com.fabianboro.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import id.com.fabianboro.myapplication.databinding.FragmentDetailMovieBinding
import id.com.fabianboro.myapplication.helper.toDateDMY
import id.com.fabianboro.myapplication.helper.viewModelsFactory
import id.com.fabianboro.myapplication.service.ApiClient
import id.com.fabianboro.myapplication.service.ApiService
import id.com.fabianboro.myapplication.viewModels.DetailMovieViewModel

class DetailMovieFragment : Fragment() {
    private var _binding: FragmentDetailMovieBinding? = null
    private val binding get() = _binding!!

    private val apiService: ApiService by lazy { ApiClient.instance }

    private val viewModel: DetailMovieViewModel by viewModelsFactory {
        DetailMovieViewModel(
            apiService
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailMovieBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val movieId = arguments?.getInt("KEY_ID", 1)
        viewModel.getDataFromNetwork(movieId!!)
        observeData()
        backButtonPressed()
    }

    private fun observeData() {
        viewModel.detailMovie.observe(viewLifecycleOwner) {
            binding.apply {
                tvTitleMovie.text = it.title
                tvTaglineMovie.text = it.tagline
                tvOverviewContent.text = it.overview
                tvReleaseDate.text = it.releaseDate.toDateDMY()
                tvOriginalTitleContent.text = it.originalTitle
                tvStatusContent.text = it.status

                val genres = it.detailMovieGenres
                val listGenre = mutableListOf<String>()
                for (index in genres.indices) {
                    val genreName = genres[index].name
                    listGenre.add(genreName)
                }
                tvGenre.text = listGenre.joinToString(", ")

                val runtime = it.runtime
                val hour = runtime / 60
                val minute = runtime - (hour * 60)
                if (hour == 0) {
                    tvRuntimeHourValue.visibility = View.GONE
                    tvRuntimeHourInfo.visibility = View.GONE
                } else {
                    tvRuntimeHourValue.text = hour.toString()
                }
                tvRuntimeMinuteValue.text = minute.toString()

                val posterLink = it.posterPath
                Glide.with(requireContext())
                    .load("https://image.tmdb.org/t/p/original$posterLink")
                    .into(binding.ivMovieImage)

                svMovie.visibility = View.VISIBLE
                pbDetailMovie.visibility = View.GONE
            }
        }
        viewModel.errorMessage.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
        }
    }

    private fun backButtonPressed() {
        binding.ivBackArrow.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}
